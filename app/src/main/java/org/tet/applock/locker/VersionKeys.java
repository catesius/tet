package org.tet.applock.locker;

abstract class VersionKeys {

    // Careful to not change this from one version to another
    /**
     * Whether ads are forced from the server or not.
     */
    public static final String ENABLE_ADS = "org.tet.applock.option.1";
}
