package org.tet.applock.locker.receivers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.PrefUtils;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class BluetoothReceiver extends BroadcastReceiver {

    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;

    public static boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        } else if (!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }
        // No need to change bluetooth state
        return true;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("", "INTENT RECEIVED: " + action);

        if (action.equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {

            SharedPreferences sp = PrefUtils.appsPrefs(context);
            boolean isBluetoothLocked = sp.getBoolean("com.bluetooth", false);

            if (isBluetoothLocked && AppLockService.isRunning(context)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_ON) {
                    if (process == 0) {
                        process = 1;
                        setBluetooth(false);
                        isEnabling = true;
                        LockService.showCompare(context, "Bluetooth", LockService.SWITCH_UNLOCK, R.drawable.bluetooth);
                    }
                    if (isUnlocked) {
                        process = 0;
                        isUnlocked = false;
                    }
                } else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_OFF) {
                    if (process == 0) {
                        process = 2;
                        setBluetooth(true);
                        isEnabling = false;
                        LockService.showCompare(context, "Bluetooth", LockService.SWITCH_UNLOCK, R.drawable.bluetooth);
                    }
                    if (isUnlocked) {
                        process = 0;
                        isUnlocked = false;
                    }
                }
            }
        } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0 && !isUnlocked) {
            setBluetooth(isEnabling);
            isUnlocked = true;
        }
    }
}
