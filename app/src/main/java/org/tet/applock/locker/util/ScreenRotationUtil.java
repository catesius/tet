package org.tet.applock.locker.util;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class ScreenRotationUtil {

    public static String ACTION_SCREEN_ROTATION = "org.tet.applock.ACTION_SCREEN_ROTATION";
    public static boolean status = false;

    public static boolean isRotationEnabled(Context c) {
        return Settings.System.getInt(c.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1;
    }

    public static void enableRotation(Context c, boolean enabled) {
        int state = enabled ? 1 : 0;
        Settings.System.putInt(c.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, state);
    }
}
