package org.tet.applock.locker.appselect;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MustafaSarper on 27.5.2015.
 */
public class AppViewHolder {

    public ImageView lock;
    public TextView name;
    public TextView description;
    public ImageView icon;
    public Drawable bg;
}
