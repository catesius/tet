package org.tet.applock.locker.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by MustafaSarper on 19.5.2015.
 */
public class CategoryUtil {

    public static String findCategory(String packageName) {
        String category = "";
        JsonParser jParser = new JsonParser();

        String url = "http://api.playstoreapi.com/v1.1/apps/" + packageName;
        // Getting JSON from URL
        JSONObject json = jParser.getJSONFromUrl(url);
        if (json == null)
            return "";
        try {
            // Getting JSON Array
            if (json.get("category") != null && !json.get("category").equals("")) {
                category = json.getString("category");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return category;
    }

}
