package org.tet.applock.locker.appselect;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import org.tet.applock.R;
import org.tet.applock.locker.Constants;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.ui.MainActivity;
import org.tet.applock.locker.util.CategoryUtil;
import org.tet.applock.locker.util.PrefUtils;
import org.tet.applock.locker.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AppAdapter extends BaseAdapter implements Filterable {

    private final Set<AppListElement> mInitialItems;
    private final LayoutInflater mInflater;
    private final PackageManager mPm;
    private final Context mContext;
    private final Editor mEditor;
    private List<AppListElement> mItems;
    private List<AppListElement> tempItems;
    private Set<String> exceptionPackages;
    private AppsFilter appsFilter;
    private ProgressDialog progress;
    private boolean mLoadComplete;
    private OnEventListener mListener;
    private boolean mDirtyState;
    /**
     * * UNDO ACTION ***
     */
    // TODO Important: Undo action.
    private List<AppListElement> mUndoItems;

    public AppAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPm = context.getPackageManager();

        mInitialItems = new HashSet<>();
        mItems = new ArrayList<>();
        mEditor = PrefUtils.appsPrefs(context).edit();
        initExceptionPackages();

        new LoaderClass().execute((Void[]) null);
    }

    // store uygulamasi degil gibi gorunen ama storeda bulunan uygulamalar icin.
    private void initExceptionPackages() {
        // todo farkli cihazlarda bakilip gerekliler eklenecek.
        exceptionPackages = new HashSet<>();
        exceptionPackages.add("com.sec.android.app.samsungapps");
        exceptionPackages.add("com.google.android.apps.maps");
    }

    @Override
    public Filter getFilter() {
        if (appsFilter == null)
            appsFilter = new AppsFilter();
        return appsFilter;
    }

    public boolean areAllAppsLocked() {
        for (AppListElement app : mItems)
            if ((app.isApp() || app.isSwitch()) && !app.locked)
                return false;
        return true;
    }

    /**
     * Creates a completely new list with the apps. Should only be called once.
     * Does not sort
     */
    void loadAppsIntoList() {
        // Get all tracked apps from preferences
        addImportantAndSystemApps(mInitialItems);

        // add all switches we can track
        addSwitches(mInitialItems);

        // get other apps with a LAUNCHER intent capability and categorize
        final Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> ris = mPm.queryIntentActivities(i, 0);

        SharedPreferences spAppCategory = PrefUtils.appCategoryPrefs(this.mContext);
        Editor editor;
        boolean foundInPrefs, doNotAdd = false, isStoreVersion = false;
        int resIdCat;
        long start = System.currentTimeMillis();
        for (ResolveInfo ri : ris) {
            foundInPrefs = true;
            if (!mContext.getPackageName().equals(ri.activityInfo.packageName)) {
                String appName = ri.loadLabel(mPm).toString();
                String packageName = ri.activityInfo.packageName;
                String category = spAppCategory.getString(packageName, "");

				/*
                     if can not find category from prefs, check if its from store
				 	and try to find category from web
				  */
                isStoreVersion = isStoreVersion(mContext, packageName);
                if (category.equals("") && isStoreVersion) {
                    foundInPrefs = false;
                    category = CategoryUtil.findCategory(packageName);

                    // change category to games, for game sub-categories
                    if (!category.equals("") && Constants.gamesContains(category))
                        category = "Games";
                }

                int priority = Constants.CategoryPriorities.PRIORITY_NORMAL_APPS.priority;
                // if found category
                if (!category.equals("")) {
                    Constants.CategoryPriorities c = Constants.CategoryPriorities.getByCategory(category);
                    if (c != null) {
                        //  get resource string from appropriate locale
                        String resString = c.category.replace("&", "").replace(" ", "").trim();
                        resIdCat = Util.getResourceIdFromString(resString);
                        priority = c.priority - 1;
                        if (resIdCat != 0) {
                            AppListElement obj = new AppListElement(mContext.getString(resIdCat), c.priority);
                            category = c.category;
                            obj.setCategory(category);
                            mInitialItems.add(obj);
                        }
                    }
                } else {
                    if (isStoreVersion)
                        doNotAdd = true;
                    // if cannot find category, try to determine
                    category = determineCategory(appName);
                    Constants.CategoryPriorities c = Constants.CategoryPriorities.getByCategory(category);

                    //  get resource string from appropriate locale
                    String resString = category.replace("&", "").replace(" ", "").trim();
                    resIdCat = Util.getResourceIdFromString(resString);
                    if (resIdCat != 0) {
                        category = c.category;
                        if (c != null) {
                            // insert category to list
                            AppListElement obj = new AppListElement(mContext.getString(resIdCat), c.priority);
                            obj.setCategory(category);
                            mInitialItems.add(obj);
                            priority = c.priority - 1;
                        }
                    }
                }

                // if a new category-app found, insert it to the prefs
                if (!foundInPrefs && !doNotAdd) {
                    editor = spAppCategory.edit();
                    editor.putString(packageName, category);
                    PrefUtils.apply(editor);
                }

                // insert application to list
                AppListElement ah = new AppListElement(appName, ri.activityInfo, priority);
                ah.setCategory(category);
                mInitialItems.add(ah);
                doNotAdd = false;
            }
        }
        long end = System.currentTimeMillis();
        long result = end - start;
        Log.e("elapsed time: ", String.valueOf(result));

        final Set<String> lockedCategories = PrefUtils.getLockedCategories(mContext);
        final Set<String> lockedApps = PrefUtils.getLockedApps(mContext);
        for (AppListElement ah : mInitialItems) {
            ah.locked = lockedApps.contains(ah.packageName);
            if (!ah.isApp() && !ah.isSwitch()) {
                ah.locked = lockedCategories.contains(ah.title);
            }
        }
        mItems = new ArrayList<>(mInitialItems);
    }

    private String determineCategory(String appName) {
        String category = Constants.CategoryPriorities.PRIORITY_NORMAL_CATEGORY.category;
        appName = appName.toLowerCase();

        if (appName.contains(mContext.getString(R.string.game)))
            category = Constants.CategoryPriorities.PRIORITY_Games_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.message))
                || appName.contains(mContext.getString(R.string.chat))
                || appName.contains(mContext.getString(R.string.mail))
                || appName.contains(mContext.getString(R.string.internet))
                || appName.contains(mContext.getString(R.string.browser))
                || appName.contains(mContext.getString(R.string.phone))
                || appName.contains(mContext.getString(R.string.call))
                || appName.contains(mContext.getString(R.string.contacts))
                || appName.contains(mContext.getString(R.string.person)))
            category = Constants.CategoryPriorities.PRIORITY_Communication_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.gallery))
                || appName.contains(mContext.getString(R.string.photo))
                || appName.contains(mContext.getString(R.string.camera)))
            category = Constants.CategoryPriorities.PRIORITY_Photography_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.music))
                || appName.contains(mContext.getString(R.string.radio))
                || appName.contains(mContext.getString(R.string.voice))
                || appName.contains(mContext.getString(R.string.video)))
            category = Constants.CategoryPriorities.PRIORITY_MusicAudio_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.memo))
                || appName.contains(mContext.getString(R.string.note))
                || appName.contains(mContext.getString(R.string.calendar))
                || appName.contains(mContext.getString(R.string.plan)))
            category = Constants.CategoryPriorities.PRIORITY_Productivity_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.magazine)))
            category = Constants.CategoryPriorities.PRIORITY_NewsMagazine_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.file)))
            category = Constants.CategoryPriorities.PRIORITY_Tools_CATEGORY.category;

        else if (appName.contains(mContext.getString(R.string.clock))
                || appName.contains(mContext.getString(R.string.alarm)))
            category = Constants.CategoryPriorities.PRIORITY_Personalization_CATEGORY.category;

        return category;
    }

    public boolean isStoreVersion(Context context, String packageName) {
        boolean result = false;
        if (exceptionPackages.contains(packageName))
            return true;
        try {
            String installer = context.getPackageManager().getInstallerPackageName(packageName);
            result = !TextUtils.isEmpty(installer);
        } catch (Exception e) {
        }

        return result;
    }

    private void addImportantAndSystemApps(Collection<AppListElement> apps) {
        // telecom for android 5.0.1
        final Set<String> incomingCall = new HashSet<>();
        incomingCall.add("com.android.phone");
        incomingCall.add("com.android.server.telecom");
        final List<String> important = Arrays.asList(new String[]{
                "com.android.vending", "com.android.settings", "com.sec.android.app.controlpanel",
                "com.android.contacts", "com.android.systemui", "com.android.packageinstaller",
                "com.android.phone", "com.android.server.telecom"});
        final List<String> importantDescriptions = Arrays.asList(new String[]{
                mContext.getString(R.string.installer_desc), mContext.getString(R.string.installer_desc_1),
                mContext.getString(R.string.installer_desc_1), mContext.getString(R.string.contacts_phone_desc),
                mContext.getString(R.string.system_ui_desc), mContext.getString(R.string.installer_desc),
                mContext.getString(R.string.incoming_call_Desc), mContext.getString(R.string.incoming_call_Desc)});

        final PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> list = pm.getInstalledApplications(0);
        boolean haveImportant = false;
        AppListElement temp;
        for (ApplicationInfo pi : list) {
            // Log.e("packagename", pi.packageName);
            if (important.contains(pi.packageName)) {
                String label = pi.loadLabel(pm).toString();
                if (incomingCall.contains(pi.packageName)) {
                    label = mContext.getString(R.string.incoming_call);
                }
                temp = new AppListElement(label, pi,
                        Constants.CategoryPriorities.PRIORITY_IMPORTANT_APPS.priority);
                temp.setCategory(Constants.CategoryPriorities.PRIORITY_IMPORTANT_CATEGORY.category);
                temp.setDescription(importantDescriptions.get(important.indexOf(pi.packageName)));
                apps.add(temp);
                haveImportant = true;
            }

            AppListElement obj = new AppListElement(mContext
                    .getString(R.string.applist_tit_apps),
                    Constants.CategoryPriorities.PRIORITY_NORMAL_CATEGORY.priority);
            obj.setCategory(Constants.CategoryPriorities.PRIORITY_NORMAL_CATEGORY.category);
            apps.add(obj);
            if (haveImportant) {
                obj = new AppListElement(mContext
                        .getString(R.string.applist_tit_important),
                        Constants.CategoryPriorities.PRIORITY_IMPORTANT_CATEGORY.priority);
                obj.setCategory(Constants.CategoryPriorities.PRIORITY_IMPORTANT_CATEGORY.category);
                apps.add(obj);
            }
        }
    }

    private void addSwitches(Collection<AppListElement> apps) {
        AppListElement temp;

        // add switches category
        temp = new AppListElement(mContext.getString(R.string.applist_tit_system), Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        apps.add(temp);

        // add wifi
        temp = new AppListElement("WiFi", "com.wifi",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.wifi);
        apps.add(temp);

        // add bluetooth
        temp = new AppListElement("Bluetooth", "com.bluetooth",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.bluetooth);
        apps.add(temp);

        // add mobile network data
        temp = new AppListElement("Mobile Network Data", "com.mobile.network",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.mobile_data);
        apps.add(temp);

        // add auto-sync
        temp = new AppListElement("Auto Sync", "com.auto.sync",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.sync_icon);
        apps.add(temp);

        // add screen rotation
        temp = new AppListElement("Screen Rotation", "com.screen.rotation",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.screen_rotation);
        apps.add(temp);

        // add wifi hotspot
        temp = new AppListElement("WiFi Hotspot", "com.wifi.hotspot",
                Constants.CategoryPriorities.PRIORITY_SWITCH_APPS.priority);
        temp.setCategory(Constants.CategoryPriorities.PRIORITY_SWITCH_CATEGORY.category);
        temp.setDescription(mContext.getString(R.string.switch_desc_text));
        temp.setIsSwitch(true);
        temp.switchIcon = mContext.getResources().getDrawable(R.drawable.wifi_hotspot);
        apps.add(temp);
    }

    /**
     * Sort the apps and notify the ListView that the items have changed. Should
     * be called from the working thread
     */
    public void sort() {
        Collections.sort(mItems);
        notifyDataSetChanged();
        notifyDirtyStateChanged(false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppListElement element = mItems.get(position);
        if (element.isApp()) {
            return createAppViewFromResource(element, convertView, parent);
        } else if (element.isSwitch()) {
            return createSwitchViewFromResource(element, convertView, parent);
        } else {
            return createSeparatorViewFromResource(element, convertView,
                    parent);
        }
    }

    private View createSwitchViewFromResource(AppListElement ah, View convertView, ViewGroup parent) {
        AppViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.applist_item_app, parent, false);
            holder = new AppViewHolder();
            holder.lock = (ImageView) view.findViewById(R.id.applist_item_image);
            holder.name = (TextView) view.findViewById(R.id.listName);
            holder.description = (TextView) view.findViewById(R.id.pro_item_description);
            holder.icon = (ImageView) view.findViewById(R.id.listIcon);

            view.setTag(holder);
        } else {
            holder = (AppViewHolder) view.getTag();
        }

        holder.lock.setVisibility(ah.locked ? View.VISIBLE : View.GONE);
        holder.name.setText(ah.title);

        if (!TextUtils.isEmpty(ah.getDescription())) {
            holder.description.setText(ah.getDescription());
        } else {
            holder.description.setVisibility(View.GONE);
        }

        holder.bg = ah.switchIcon;
        if (holder.bg == null)
            holder.icon.setVisibility(View.GONE);
        else
            setBackgroundCompat(holder.icon, holder.bg);

        return view;
    }

    private View createSeparatorViewFromResource(AppListElement ah, View convertView, ViewGroup parent) {
        CategoryViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.applist_item_category, parent,
                    false);
            holder = new CategoryViewHolder();
            holder.lock = (ImageView) view.findViewById(R.id.applist_item_image);
            holder.text = (TextView) view.findViewById(R.id.listName);

            view.setTag(holder);
        } else {
            holder = (CategoryViewHolder) view.getTag();
        }

        holder.lock.setVisibility(ah.locked ? View.VISIBLE : View.GONE);
        holder.text.setText(ah.title);

        return view;
    }

    private View createAppViewFromResource(AppListElement ah, View convertView, ViewGroup parent) {
        AppViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.applist_item_app, parent, false);
            holder = new AppViewHolder();
            holder.lock = (ImageView) view.findViewById(R.id.applist_item_image);
            holder.name = (TextView) view.findViewById(R.id.listName);
            holder.description = (TextView) view.findViewById(R.id.pro_item_description);
            holder.icon = (ImageView) view.findViewById(R.id.listIcon);

            view.setTag(holder);
        } else {
            holder = (AppViewHolder) view.getTag();
        }

        holder.lock.setVisibility(ah.locked ? View.VISIBLE : View.GONE);

        if (!TextUtils.isEmpty(ah.getDescription())) {
            holder.description.setText(ah.getDescription());
        } else {
            holder.description.setVisibility(View.GONE);
        }

        holder.name.setText(ah.getLabel(mPm));
        holder.bg = ah.getIcon(mPm);
        if (holder.bg == null)
            holder.icon.setVisibility(View.GONE);
        else
            setBackgroundCompat(holder.icon, holder.bg);

        return view;
    }

    public void prepareUndo() {
        //mUndoItems = new ArrayList<>(mItems);
    }

    public void undo() {
        mItems = new ArrayList<>(mUndoItems);
        notifyDataSetChanged();
    }

    /**
     * * LOCK/UNLOCK ACTIONS ***
     */
    // lock/unlock all apps
    public void setAllLocked(boolean lock) {
        ArrayList<String> apps = new ArrayList<>();
        Editor catEditor = PrefUtils.appCategoryLockPrefs(mContext).edit();
        for (AppListElement app : mItems) {
            app.locked = lock;
            if (app.isApp() || app.isSwitch()) {
                apps.add(app.packageName);
            } else {
                if (lock) {
                    catEditor.putBoolean(app.getCategory(), true);
                } else {
                    catEditor.remove(app.getCategory());
                }
            }
        }
        setLocked(lock, apps.toArray(new String[apps.size()]));
        sort();
        save();
        PrefUtils.apply(catEditor);
    }

    // lock/unlock all apps in category
    public void setAllCategoryLocked(boolean lock, String category) {
        ArrayList<String> apps = new ArrayList<>();
        Editor editor = PrefUtils.appCategoryLockPrefs(mContext).edit();
        if (lock) {
            editor.putBoolean(category, true);
        } else {
            editor.remove(category);
        }
        for (AppListElement app : tempItems) {
            if (category.equals(app.getCategory())) {
                app.locked = lock;
                apps.add(app.packageName);
            }
        }
        setLocked(lock, apps.toArray(new String[apps.size()]));
        sort();
        save();
        PrefUtils.apply(editor);
        notifyDataSetChanged();
    }

    // toggle an app, also check for category to be locked/unlocked
    public void toggle(AppListElement item) {
        if (item.isApp() || item.isSwitch()) {
            item.locked = !item.locked;
            setLocked(item.locked, item.packageName);
            save();
            Editor catEditor = PrefUtils.appCategoryLockPrefs(mContext).edit();
            if (!item.locked) {
                catEditor.remove(item.getCategory());
                for (AppListElement al : tempItems) {
                    if (item.getCategory().equals(al.getCategory())) {
                        al.locked = false;
                        break;
                    }
                }
            } else {
                List<AppListElement> lockList = new ArrayList<>();
                for (AppListElement al : tempItems) {
                    if ((al.isApp() || al.isSwitch()) && item.getCategory().equals(al.getCategory())) {
                        lockList.add(al);
                    }
                }
                int lockCount = 0;
                for (AppListElement al : lockList) {
                    if (al.locked) {
                        lockCount++;
                    }
                }
                if (lockCount == lockList.size()) {
                    for (AppListElement al : mItems) {
                        if (al.getCategory() != null && al.getCategory().equals(item.getCategory())) {
                            al.locked = true;
                            break;
                        }
                    }
                    catEditor.putBoolean(item.getCategory(), true);
                }
            }
            PrefUtils.apply(catEditor);
            notifyDataSetChanged();
        }
        List<AppListElement> list = new ArrayList<>(mItems);
        Collections.sort(list);
        boolean dirty = !list.equals(mItems);
        Log.d("", "dirty=" + dirty + ", mDirtyState = " + mDirtyState);

        notifyDirtyStateChanged(dirty);
    }

    void setLocked(boolean lock, String... packageNames) {
        Log.d("", "setLocked");
        for (String packageName : packageNames) {
            if (lock) {
                mEditor.putBoolean(packageName, true);
            } else {
                mEditor.remove(packageName);
            }
        }
    }

    void save() {
        PrefUtils.apply(mEditor);
        AppLockService.restart(mContext);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void setBackgroundCompat(View v, Drawable bg) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
            v.setBackgroundDrawable(bg);
        else
            v.setBackground(bg);
    }

    public void setItemsToDefault() {
        mItems = new ArrayList<>(tempItems);
        notifyDataSetChanged();
    }

    public boolean isLoadComplete() {
        return mLoadComplete;
    }

    public void setOnEventListener(OnEventListener listener) {
        mListener = listener;
    }

    private void notifyDirtyStateChanged(boolean dirty) {
        if (mDirtyState != dirty) {
            mDirtyState = dirty;
            if (mListener != null) {
                mListener.onDirtyStateChanged(dirty);
            }
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    public List<AppListElement> getAllItems() {
        return mItems;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        // Number of different views we have
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).isApp() ? 0 : mItems.get(position).isSwitch() ? 2 : 1;
    }

    public interface OnEventListener {
        void onLoadComplete();

        void onDirtyStateChanged(boolean dirty);
    }

    private class LoaderClass extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(mContext);
            progress.setTitle(mContext.getString(R.string.loaderTitle));
            progress.setMessage(mContext.getString(R.string.loaderMessage));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            loadAppsIntoList();
            tempItems = new ArrayList<>(mItems);
            Collections.sort(tempItems);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            sort();
            if (mListener != null) {
                mLoadComplete = true;
                mListener.onLoadComplete();
            }
            progress.dismiss();

            // load tamamlandiginda baska seyler yapilmak istenirse bu intent yakalanabilir.
            Intent i = new Intent();
            i.setAction(MainActivity.LOAD_COMPLETE);
            mContext.sendBroadcast(i);
        }

    }

    /**
     * * FILTER/SEARCH ACTION ***
     */
    private class AppsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<AppListElement> tempList = new ArrayList<>();

                // search content in friend list
                for (AppListElement app : tempItems) {
                    if (app.title.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(app);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = tempItems.size();
                filterResults.values = tempItems;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.values != null)
                mItems = (ArrayList<AppListElement>) results.values;
            notifyDataSetChanged();
        }
    }

}
