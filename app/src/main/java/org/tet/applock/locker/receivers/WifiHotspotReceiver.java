package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.PrefUtils;

import java.lang.reflect.Method;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class WifiHotspotReceiver extends BroadcastReceiver {

    public static final String TAG = "wifi hotspot state";
    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;

    //check whether wifi hotspot on or off
    public static boolean isApOn(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        } catch (Exception ignored) {
        }
        return false;
    }

    // toggle wifi hotspot on or off
    public static boolean configApState(Context context, boolean enabled) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        WifiConfiguration wificonfiguration = null;
        try {
            Method method = wifimanager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifimanager, wificonfiguration, enabled);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("", "INTENT RECEIVED: " + action);

        if (action.equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        // todo 4.0 oncesinde calismayabilir

        SharedPreferences sp = PrefUtils.appsPrefs(context);
        boolean isWifiHotspotLocked = sp.getBoolean("com.wifi.hotspot", false);

        if (isWifiHotspotLocked && AppLockService.isRunning(context)) {
            if ("android.net.wifi.WIFI_AP_STATE_CHANGED".equals(action)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                switch (state) {
                    case 11: // WIFI_AP_STATE_DISABLED
                        Log.d(TAG, "disabled");
                        if (isUnlocked) {
                            process = 0;
                            isUnlocked = false;
                        } else if (process == 1) {
                            LockService.showCompare(context, "WiFi Hotspot", LockService.SWITCH_UNLOCK, R.drawable.wifi_hotspot);
                        }
                        break;
                    case 13: // WIFI_AP_STATE_ENABLED
                        Log.d(TAG, "enabled");
                        if (isUnlocked) {
                            process = 0;
                            isUnlocked = false;
                        } else if (process == 2) {
                            LockService.showCompare(context, "WiFi Hotspot", LockService.SWITCH_UNLOCK, R.drawable.wifi_hotspot);
                        }
                        break;
                    case 10: // WIFI_AP_STATE_DISABLING
                        Log.d(TAG, "disabling");
                        if (process == 0) {
                            process = 2;
                            isEnabling = false;
                            configApState(context, true);
                        }
                        break;
                    case 12: // WIFI_AP_STATE_ENABLING
                        Log.d(TAG, "enabling");
                        if (process == 0) {
                            process = 1;
                            isEnabling = true;
                            configApState(context, false);
                        }
                        break;
                    default:
                        break;
                }
            } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0) {
                configApState(context, isEnabling);
                isUnlocked = true;
            }
        }
    }
}
