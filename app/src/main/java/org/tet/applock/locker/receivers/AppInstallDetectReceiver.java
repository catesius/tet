package org.tet.applock.locker.receivers;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.util.Log;
import android.view.WindowManager;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.util.PrefUtils;

import java.util.List;

/**
 * Created by Cihad on 6.5.2015.
 */
public class AppInstallDetectReceiver extends BroadcastReceiver {

    public static boolean checkInstalledApp = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        PrefUtils prefs = new PrefUtils(context);
        checkInstalledApp = prefs.getBoolean(R.string.pref_key_lock_new_app, R.bool.pref_def_lock_new_app);
        prefs = null;

        if (!checkInstalledApp) {
            return;
        }

        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED") && !intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
            String[] a = intent.getData().toString().split(":");
            String packageName = a[a.length - 1];
            String appName = null;
            List<PackageInfo> packageInfoList = context.getPackageManager().getInstalledPackages(0);
            for (int x = 0; x < packageInfoList.size(); x++) {
                PackageInfo packageInfo = packageInfoList.get(x);
                if (packageInfo.packageName.equals(packageName)) {
                    appName = packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString();
                }
            }
            boolean locked = PrefUtils.appsPrefs(context).getBoolean(packageName, false);
            if (!locked)
                displayAlert(context, appName, packageName);

        }
    }

    public void displayAlert(final Context context, String appName, final String packageName) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(R.string.application_name);
        String temp = appName + " " + context.getString(R.string.appInstallDetectReceiver_dialog);
        // set dialog message
        alertDialogBuilder
                .setMessage(temp)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.appInstallDetectReceiver_dialog_onay), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        Log.i("Warn", "uygulama şifrelenecek");
                        SharedPreferences.Editor mEditor = PrefUtils.appsPrefs(context).edit();
                        mEditor.putBoolean(packageName, true);
                        PrefUtils.apply(mEditor);
                        AppLockService.restart(context);
                        dialog.cancel();
                    }
                })
                .setNegativeButton(context.getString(R.string.appInstallDetectReceiver_dialog_red), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        Log.i("Warn", "uygulama şifre listesine eklenmeyecek");
//                        Gerekir mi?
//                        SharedPreferences.Editor mEditor = PrefUtils.appsPrefs(context).edit();
//                        mEditor.putBoolean(packageName, false);
//                        PrefUtils.apply(mEditor);
//                        AppLockService.restart(context);
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        //bu type verilmezse alert dialogu receiverde açamıyoruz.
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        // show it
        alertDialog.show();
    }
}
