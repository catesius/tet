package org.tet.applock.locker.appselect;

import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

/**
 * Represents a Application or a Separator
 *
 * @author TET
 */
public class AppListElement implements Comparable<AppListElement> {

    // null if not an activity
    public final String packageName;
    // null if not an activity
    private final PackageItemInfo pii;
    /**
     * Indicates the priority of this item. The higher the priority, the higher
     * it will appear in the list
     */
    private final int priority;
    public String title;
    public Drawable switchIcon;
    public boolean locked = false;
    private Drawable mIcon;
    private String category;
    private String description;
    private boolean isSwitch;

    public AppListElement(String label, PackageItemInfo pii, int priority) {
        this.title = label;
        this.pii = pii;
        this.packageName = pii.packageName;
        this.priority = priority;
    }

    /**
     * For separators
     */
    public AppListElement(String label, int priority) {
        this.title = label;
        this.pii = null;
        this.packageName = "";
        this.priority = priority;

    }

    /**
     * For non activity apps
     */
    public AppListElement(String label, String packageName, int priority) {
        this.title = label;
        this.pii = null;
        this.packageName = packageName;
        this.priority = priority;

    }

    public Drawable getIcon(PackageManager pm) {
        if (mIcon == null) {
            if (pii == null)
                return null;
            mIcon = pii.loadIcon(pm);
        }
        return mIcon;
    }

    public String getLabel(PackageManager pm) {
        if (title == null) {
            title = (String) pii.loadLabel(pm);
        }
        return title;
    }

    public boolean isApp() {
        return !isSwitch() && packageName != null && packageName.length() > 0;
    }

    @Override
    public final boolean equals(Object object) {
        if (object == null)
            return false;
        if (!(object instanceof AppListElement))
            return false;
        AppListElement sh = (AppListElement) object;
        if (isApp() != sh.isApp())
            return false;
        if (isSwitch() != sh.isSwitch())
            return false;
        if (!isApp() && !isSwitch()) {
            return title != null && title.equals(sh.title);
        }
        return packageName != null && packageName.equals(sh.packageName);
    }

    @Override
    public int hashCode() {
        if (isApp() || isSwitch()) {
            return new StringBuilder("bypkgname").append(packageName)
                    .toString().hashCode();
        }
        return new StringBuilder("bytitle").append(title).toString().hashCode();
    }

    @Override
    public int compareTo(AppListElement o) {
        if (priority != o.priority)
            return o.priority - priority;

        if (this.locked != o.locked)
            return this.locked ? -1 : 1;
        if (this.title == null || o.title == null) {
            return 0;
        }
        return this.title.compareTo(o.title);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSwitch() {
        return isSwitch;
    }

    public void setIsSwitch(boolean isSwitch) {
        this.isSwitch = isSwitch;
    }
}
