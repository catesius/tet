package org.tet.applock.locker;

import java.util.HashSet;
import java.util.Set;

public final class Constants {

    public static final boolean DEBUG = false;

    public static final int APPS_PER_INTERSTITIAL = 1;

    public static Set<String> games;

    static {
        if (games == null) {
            games = new HashSet<>();
            games.add("Action");
            games.add("Adventure");
            games.add("Arcade");
            games.add("Board");
            games.add("Card");
            games.add("Casino");
            games.add("Casual");
            games.add("Educational");
            games.add("Family");
            games.add("Music");
            games.add("Puzzle");
            games.add("Racing");
            games.add("Role Playing");
            games.add("Simulation");
            games.add("Games/Sports");
            games.add("Strategy");
            games.add("Trivia");
            games.add("Word");
        }
    }

    public static boolean gamesContains(String cat) {
        return games.contains(cat);
    }

    // app categories
    // TODO get strings from file
    public enum CategoryPriorities {
        PRIORITY_IMPORTANT_APPS(99, ""),
        PRIORITY_IMPORTANT_CATEGORY(100, "Important"),
        PRIORITY_SWITCH_APPS(97, ""),
        PRIORITY_SWITCH_CATEGORY(98, "Switches"),
        PRIORITY_NORMAL_APPS(55, ""),
        PRIORITY_NORMAL_CATEGORY(56, "Other Apps"),
        PRIORITY_Games_APPS(53, ""),
        PRIORITY_Games_CATEGORY(54, "Games"),
        PRIORITY_BooksRef_APPS(51, ""),
        PRIORITY_BooksRef_CATEGORY(52, "Books & Reference"),
        PRIORITY_Business_APPS(49, ""),
        PRIORITY_Business_CATEGORY(50, "Business"),
        PRIORITY_Comics_APPS(47, ""),
        PRIORITY_Comics_CATEGORY(48, "Comics"),
        PRIORITY_Communication_APPS(45, ""),
        PRIORITY_Communication_CATEGORY(46, "Communication"),
        PRIORITY_Education_APPS(43, ""),
        PRIORITY_Education_CATEGORY(44, "Education"),
        PRIORITY_Entertainment_APPS(41, ""),
        PRIORITY_Entertainment_CATEGORY(42, "Entertainment"),
        PRIORITY_Finance_APPS(39, ""),
        PRIORITY_Finance_CATEGORY(40, "Finance"),
        PRIORITY_HealthFitness_APPS(37, ""),
        PRIORITY_HealthFitness_CATEGORY(38, "Health & Fitness"),
        PRIORITY_LibrariesDemo_APPS(35, ""),
        PRIORITY_LibrariesDemo_CATEGORY(36, "Libraries & Demo"),
        PRIORITY_Lifestyle_APPS(33, ""),
        PRIORITY_Lifestyle_CATEGORY(34, "Lifestyle"),
        PRIORITY_LiveWallpaper_APPS(31, ""),
        PRIORITY_LiveWallpaper_CATEGORY(32, "Live Wallpaper"),
        PRIORITY_MediaVideo_APPS(29, ""),
        PRIORITY_MediaVideo_CATEGORY(30, "Media & Video"),
        PRIORITY_Medical_APPS(27, ""),
        PRIORITY_Medical_CATEGORY(28, "Medical"),
        PRIORITY_MusicAudio_APPS(25, ""),
        PRIORITY_MusicAudio_CATEGORY(26, "Music & Audio"),
        PRIORITY_NewsMagazine_APPS(23, ""),
        PRIORITY_NewsMagazine_CATEGORY(24, "News & Magazines"),
        PRIORITY_Personalization_APPS(21, ""),
        PRIORITY_Personalization_CATEGORY(22, "Personalization"),
        PRIORITY_Photography_APPS(19, ""),
        PRIORITY_Photography_CATEGORY(20, "Photography"),
        PRIORITY_Productivity_APPS(17, ""),
        PRIORITY_Productivity_CATEGORY(18, "Productivity"),
        PRIORITY_Shopping_APPS(15, ""),
        PRIORITY_Shopping_CATEGORY(16, "Shopping"),
        PRIORITY_Social_APPS(13, ""),
        PRIORITY_Social_CATEGORY(14, "Social"),
        PRIORITY_Sports_APPS(11, ""),
        PRIORITY_Sports_CATEGORY(12, "Sports"),
        PRIORITY_Tools_APPS(9, ""),
        PRIORITY_Tools_CATEGORY(10, "Tools"),
        PRIORITY_Transportation_APPS(7, ""),
        PRIORITY_Transportation_CATEGORY(8, "Transportation"),
        PRIORITY_TravelLocal_APPS(5, ""),
        PRIORITY_TravelLocal_CATEGORY(6, "Travel & Local"),
        PRIORITY_Weather_APPS(3, ""),
        PRIORITY_Weather_CATEGORY(4, "Weather"),
        PRIORITY_Widgets_APPS(1, ""),
        PRIORITY_Widgets_CATEGORY(2, "Widgets");

        public int priority;
        public String category;

        CategoryPriorities(int priority, String category) {
            this.priority = priority;
            this.category = category;
        }

        public static CategoryPriorities getByCategory(String category) {
            for (CategoryPriorities cp : CategoryPriorities.values()) {
                if (cp.category.equals(category)) {
                    return cp;
                }
            }
            return null;
        }

        public static CategoryPriorities getByPriority(int priority) {
            for (CategoryPriorities cp : CategoryPriorities.values()) {
                if (cp.priority == priority) {
                    return cp;
                }
            }
            return null;
        }


    }

}
