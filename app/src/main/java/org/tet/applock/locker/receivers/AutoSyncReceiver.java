package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.AutoSyncUtil;
import org.tet.applock.locker.util.PrefUtils;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class AutoSyncReceiver extends BroadcastReceiver {

    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("", "INTENT RECEIVED: " + action);

        if (action.equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        SharedPreferences sp = PrefUtils.appsPrefs(context);
        boolean isAutoSyncLocked = sp.getBoolean("com.auto.sync", false);

        if (isAutoSyncLocked && AppLockService.isRunning(context)) {
            if (action.equals(AutoSyncUtil.AUTO_SYNC_CHANGE)) {
                Log.d("auto sync", "change");
                boolean status = AutoSyncUtil.status;

                if (process == 0) {
                    process = 1;
                    AutoSyncUtil.setEnabled(!status);
                    isEnabling = status;
                    LockService.showCompare(context, "Auto Sync", LockService.SWITCH_UNLOCK, R.drawable.sync_icon);
                }
                if (isUnlocked) {
                    process = 0;
                    isUnlocked = false;
                }
            } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0 && !isUnlocked) {
                AutoSyncUtil.setEnabled(isEnabling);
                isUnlocked = true;
            }
        }
    }


}
