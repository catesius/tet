package org.tet.applock.locker.receivers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.ui.MainActivity;

/**
 * Created by MustafaSarper on 17.5.2015.
 */
public class WidgetReceiver extends AppWidgetProvider {

    public static String CHANGE_LOCK = "android.LockWidget.CHANGE_LOCK";
    private static boolean askedPassword = false;
    RemoteViews remoteViews;
    AppWidgetManager appWidgetManager;
    ComponentName thisWidget;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        boolean isServiceRunning = AppLockService.isRunning(context);
        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            this.appWidgetManager = appWidgetManager;

            updateSwitch(context, appWidgetManager, appWidgetId, isServiceRunning);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("widget intent", intent.getAction());
        super.onReceive(context, intent);

        RemoteViews remoteViews;
        AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));
        final int N = appWidgetIds.length;
        boolean serviceRunning = AppLockService.isRunning(context);
        for (int i = 0; i < N; i++) {
            Log.e("widget", "" + appWidgetIds[i]);
            remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget);

            if (intent.getAction().equals(CHANGE_LOCK)) {

                if (serviceRunning && !askedPassword) {
                    askedPassword = true;
                    // call password view
                    LockService.showCompare(context, MainActivity.PACKAGE_NAME, LockService.SERVICE_UNLOCK, R.drawable.ic_launcher);
                } else {
                    remoteViews.setImageViewResource(R.id.lock_image,
                            R.drawable.ic_action_secure);
                    AppLockService.start(context);
                }
            }

            if (intent.getAction().equals(AppLockService.ACTION_START)) {
                remoteViews.setImageViewResource(R.id.lock_image,
                        R.drawable.ic_action_secure);
            } else if (intent.getAction().equals(AppLockService.ACTION_STOP)) {
                remoteViews.setImageViewResource(R.id.lock_image,
                        R.drawable.ic_action_not_secure);
            } else if (intent.getAction().equals(LockService.SERVICE_UNLOCK) && askedPassword) {
                remoteViews.setImageViewResource(R.id.lock_image,
                        R.drawable.ic_action_not_secure);
                AppLockService.stop(context);
                askedPassword = false;
            }
            appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);
        }
    }

    private void updateSwitch(Context context, AppWidgetManager appWidgetManager, int appWidgetId, boolean isServiceRunning) {
        Log.d("SWitch Widget", "Switch1 " + appWidgetId);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget);

        Intent imageview1Intent = new Intent(context, WidgetReceiver.class);
        imageview1Intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        imageview1Intent.setAction(CHANGE_LOCK);
        PendingIntent imageview1PendingIntent = PendingIntent.getBroadcast(context,
                0, imageview1Intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.lock_image, imageview1PendingIntent);

        if (isServiceRunning) {
            remoteViews.setImageViewResource(R.id.lock_image, R.drawable.ic_action_secure);
        } else {
            remoteViews.setImageViewResource(R.id.lock_image, R.drawable.ic_action_not_secure);
        }

        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }

}
