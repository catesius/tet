package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.PrefUtils;

public class NetworkStateReceiver extends BroadcastReceiver {

    private static String TAG = "wifi state";
    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;
    private static boolean status = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("", "INTENT RECEIVED: " + intent.getAction());

        if (intent.getAction().equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
            SharedPreferences sp = PrefUtils.appsPrefs(context);
            boolean isWifiLocked = sp.getBoolean("com.wifi", false);

            if (isWifiLocked && AppLockService.isRunning(context)) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                if(state == WifiManager.WIFI_STATE_DISABLING || state == WifiManager.WIFI_STATE_ENABLING)
                    return;
                status = WifiManager.WIFI_STATE_ENABLED == state;

                if (process == 0) {
                    process = 1;
                    wifiManager.setWifiEnabled(!status);
                    isEnabling = status;
                    LockService.showCompare(context, "WiFi", LockService.SWITCH_UNLOCK, R.drawable.wifi);
                }
                if (isUnlocked) {
                    process = 0;
                    isUnlocked = false;
                }
            }
        } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(isEnabling);
            isUnlocked = true;
        }
    }

    // private String unescapeSsid(String ssid) {
    // if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
    // ssid = ssid.substring(1, ssid.length() - 1);
    // }
    // return ssid;
    // }

    // Old method, do not use.

    // private static final void processConnectivityEvent(Intent intent, Context
    // c) {
    // String a = WifiManager.NETWORK_STATE_CHANGED_ACTION;
    // SharedPreferences sp = c.getSharedPreferences("beta",
    // Context.MODE_PRIVATE);
    // // TODO bug here
    // final boolean should = sp.getBoolean(
    // c.getString(R.string.pref_key_temp_wifi_unlock_state), false);
    // if (!should)
    // return;
    //
    // String savedSSID = sp.getString(
    // c.getString(R.string.pref_key_temp_wifi_ssid), "");
    // NetworkInfo ni = (NetworkInfo) intent
    // .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
    //
    // if (ni.isConnected()) {
    // switch (ni.getType()) {
    // case ConnectivityManager.TYPE_WIFI:
    // WifiManager wm = (WifiManager) c
    // .getSystemService(Context.WIFI_SERVICE);
    // WifiInfo wi = wm.getConnectionInfo();
    // String ssid = wi.getSSID();
    //
    // Log.d("RECEIVER", "SSID: " + ssid + " saved: " + savedSSID);
    // if (ssid != null && ssid.length() != 0) {
    // // 4.2+ have quotes, remove them if it has!
    // if (ssid.equals(savedSSID)) {
    // Log.d("RECEIVER", "Stopping service, ssid matched");
    // Intent i = new Intent(c, AppLockService.class);
    // c.stopService(i);
    // }
    // }
    // break;
    // }
    // }
    // }

}
